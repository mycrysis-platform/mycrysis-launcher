// Copyright (C) 2019-2020 MYCRYSIS(Unofficial) NET TEAM.
// All rights reserved. Contacts: <stanislav.migunov@outlook.com>
// Created by Stanislav Migunov
package net.mycrysis.launcher.ViewModel

import de.saxsys.mvvmfx.ViewModel
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleDoubleProperty
import javafx.beans.property.SimpleStringProperty
import javafx.beans.property.StringProperty
import javafx.scene.control.Alert
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.javafx.JavaFx
import kotlinx.coroutines.launch
import net.mycrysis.launcher.LauncherObject
import net.mycrysis.launcher.http.HttpDownloader

class RootViewModel: ViewModel {
    companion object {
        val SIGN_IN_ACTION = "SIGN_IN_ACTION"
        val SIGN_OUT_ACTION = "SIGN_OUT_ACTION"
    }

    //No Localized Text Labels
    val gameBuildProperty: StringProperty = SimpleStringProperty(LauncherObject.gameBuild)
    val launcherVersionProperty: StringProperty = SimpleStringProperty(LauncherObject.config.launcherVersion)
    val usernameProperty: StringProperty = SimpleStringProperty(LauncherObject.username)

    //LocalizedLabels
    val gameButtonLable: StringProperty = SimpleStringProperty(if(LauncherObject.isGameInstalled) "Play" else "Install Game")
    val discordButtonLable: StringProperty = SimpleStringProperty("Sing In via Discord")
    val exitButtonLable: StringProperty = SimpleStringProperty("Exit")
    val progressBarLable: StringProperty = SimpleStringProperty(" ")

    //Buttons
    val discordButtonVisibility = SimpleBooleanProperty(true)
    val gameButtonDisable = SimpleBooleanProperty(false)
    val exitAccountButtonDisable = SimpleBooleanProperty(true)

    //ProgressBar
    val progressBarValue = SimpleDoubleProperty(1.0)


    fun downloadGame(path: String, callback: (gamepath: String) -> Unit) {
        progressBarValue.value = 0.0

        exitAccountButtonDisable.set(true)
        gameButtonLable.set("Downloading...")

        val onDownloading = { status: String, progress: Double ->
            progressBarLable.set(status)
            progressBarValue.value = progress

            if(progress >= 1.0)
                progressBarLable.set("")
        }

        HttpDownloader.downloadBuild(path, onDownloading) { failed, msg ->
            GlobalScope.launch(Dispatchers.JavaFx) {
                when(failed) {
                    true -> {
                        exitAccountButtonDisable.set(false)
                        gameButtonDisable.set(false)

                        gameButtonLable.set("Install")

                        progressBarLable.set("")
                        progressBarValue.value = 1.0

                        // TODO -> Localization
                        LauncherObject.showAlertAndWait(Alert.AlertType.ERROR, "Download error", null, msg)
                    }
                    false -> {
                        exitAccountButtonDisable.set(false)
                        gameButtonDisable.set(false)

                        gameButtonLable.set("Play")

                        callback(path)
                    }
                }
            }
        }
    }
}