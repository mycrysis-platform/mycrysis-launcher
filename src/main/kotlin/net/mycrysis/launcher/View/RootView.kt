// Copyright (C) 2019-2020 MYCRYSIS(Unofficial) NET TEAM.
// All rights reserved. Contacts: <stanislav.migunov@outlook.com>
// Created by Stanislav Migunov
package net.mycrysis.launcher.View

import de.saxsys.mvvmfx.FxmlView
import de.saxsys.mvvmfx.InjectViewModel
import de.saxsys.mvvmfx.MvvmFX
import javafx.event.ActionEvent
import javafx.fxml.FXML
import javafx.fxml.Initializable
import javafx.scene.control.Alert
import javafx.scene.control.Button
import javafx.scene.control.ProgressBar
import javafx.scene.layout.AnchorPane
import javafx.scene.text.Text
import javafx.stage.DirectoryChooser
import javafx.stage.Stage
import net.mycrysis.launcher.LauncherObject
import net.mycrysis.launcher.MD5Checker
import net.mycrysis.launcher.ViewModel.RootViewModel
import java.awt.Desktop
import java.io.File
import java.net.URL
import java.util.*

class RootView: FxmlView<RootViewModel>, Initializable {
    @FXML
    private lateinit var background: AnchorPane

    @FXML
    private lateinit var gameBuildLable: Text

    @FXML
    private lateinit var launcherVersionLable: Text

    @FXML
    private lateinit var usernameLable: Text

    @FXML
    private lateinit var discordButton: Button

    @FXML
    private lateinit var gameButton: Button

    @FXML
    private lateinit var exitAccountButton: Button

    @FXML
    private lateinit var progressBar: ProgressBar

    @FXML
    private lateinit var progressStatusLabel: Text

    @InjectViewModel
    private lateinit var rootViewModel: RootViewModel

    private val notificationCenter =  MvvmFX.getNotificationCenter()

    private val urlDiscord: String
        get() = "https://discord.com/api/oauth2/authorize?client_id=756598414854324316&redirect_uri=http%3A%2F%2F127.0.0.1%3A30333%2Fmycrysis%2Flauncher&response_type=code&scope=identify"

    override fun initialize(location: URL?, resources: ResourceBundle?) {
        gameBuildLable.textProperty().bind(rootViewModel.gameBuildProperty)
        launcherVersionLable.textProperty().bind(rootViewModel.launcherVersionProperty)
        usernameLable.textProperty().bind(rootViewModel.usernameProperty)

        gameButton.textProperty().bind(rootViewModel.gameButtonLable)
        discordButton.textProperty().bind(rootViewModel.discordButtonLable)
        exitAccountButton.textProperty().bind(rootViewModel.exitButtonLable)

        discordButton.visibleProperty().bind(rootViewModel.discordButtonVisibility)
        gameButton.disableProperty().bind(rootViewModel.gameButtonDisable)
        exitAccountButton.disableProperty().bind(rootViewModel.exitAccountButtonDisable)

        progressBar.progressProperty().bind(rootViewModel.progressBarValue)
        progressStatusLabel.textProperty().bind(rootViewModel.progressBarLable)

    }

    @FXML
    private fun onSignInDiscordAction(event: ActionEvent) = try {
        Desktop.getDesktop().browse(
            URL(urlDiscord).toURI()
        )

        notificationCenter.subscribe(RootViewModel.SIGN_IN_ACTION) { _: String, payload: Array<Any> ->
            val username = payload[0] as String

            rootViewModel.usernameProperty.set(username)

            rootViewModel.discordButtonVisibility.set(false)
            rootViewModel.gameButtonDisable.set(false)
            rootViewModel.exitAccountButtonDisable.set(false)

            notificationCenter.unsubscribe(RootViewModel.SIGN_IN_ACTION, null)
        }
    } catch (e: Exception) {
        println(e.message)
    }

    @FXML
    private fun onExitFromAccountAction(event: ActionEvent) {
        rootViewModel.usernameProperty.set("NO USER")
        rootViewModel.discordButtonVisibility.set(true)
        rootViewModel.gameButtonDisable.set(true)
        rootViewModel.exitAccountButtonDisable.set(true)
    }

    @FXML
    private fun onGameButtonAction(event: ActionEvent) {
        rootViewModel.gameButtonDisable.set(true)
        rootViewModel.gameButtonLable.set("Initializing...")

        if(!LauncherObject.isGameInstalled) {
            val stage = background.scene.window as Stage

            val directoryChooser = DirectoryChooser()
            val selectedDirectory: File? = directoryChooser.showDialog(stage)

            if(selectedDirectory == null) {
                rootViewModel.gameButtonLable.set("Install Game")
                rootViewModel.gameButtonDisable.set(false)

                return
            }

            rootViewModel.downloadGame(selectedDirectory.path) {
                LauncherObject.isGameInstalled = true
                LauncherObject.config.buildDirectory = it

                launchGame()
            }

        } else if(LauncherObject.isGameInstalled) {
            launchGame()
        }
    }

    private fun launchGame() {
        rootViewModel.gameButtonLable.set("Launching...")
        rootViewModel.exitAccountButtonDisable.set(true)

        rootViewModel.progressBarLable.set("")

        MD5Checker.gameCheck { status: String, complete: Boolean, failed: Boolean ->
            rootViewModel.progressBarLable.set(status)
            when {
                complete -> {
                    rootViewModel.progressBarLable.set("")

                    if(!osRunnable())
                        return@gameCheck


                }
                failed -> {
                    // TODO -> Localization
                    rootViewModel.gameButtonLable.set("Verify error")
                    rootViewModel.gameButtonDisable.set(false)
                    //rootViewModel.progressBarLable.set("Game Content verify error")

                    return@gameCheck
                }
            }


        }
    }

    private fun osRunnable():Boolean {
        val os = System.getProperty("os.name")
        if(os != "Windows") {
            rootViewModel.gameButtonLable.set("Not Supported")
            // TODO -> Localization
            LauncherObject.showAlertAndWait(
                Alert.AlertType.ERROR,
                "OS not Supported",
                "OS != OS.Windows",
                "Your system is not supported by game."
            )

            return false
        }

        return true
    }
}