// Copyright (C) 2019-2020 MYCRYSIS(Unofficial) NET TEAM.
// All rights reserved. Contacts: <stanislav.migunov@outlook.com>
// Created by Stanislav Migunov
package net.mycrysis.launcher.dto

import com.google.gson.annotations.SerializedName

data class GameContentModel(
    @SerializedName("dir")
    val directory: String,

    @SerializedName("name")
    val filename: String,

    @SerializedName("format")
    val file_format: String,

    @SerializedName("md5")
    val md5: String
)
