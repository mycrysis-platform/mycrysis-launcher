// Copyright (C) 2019-2020 MYCRYSIS(Unofficial) NET TEAM.
// All rights reserved. Contacts: <stanislav.migunov@outlook.com>
// Created by Stanislav Migunov
package net.mycrysis.launcher.dto

import com.google.gson.annotations.SerializedName

data class DiscordAccessData(
    @SerializedName("access_token")
    val accessToken: String,

    @SerializedName("expires_in")
    val expiresIn: Int,

    @SerializedName("refresh_token")
    val refreshToken: String,

    @SerializedName("scope")
    val scope: String,

    @SerializedName("token_type")
    val tokenType: String
)