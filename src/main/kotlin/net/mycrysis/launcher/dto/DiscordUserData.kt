// Copyright (C) 2019-2020 MYCRYSIS(Unofficial) NET TEAM.
// All rights reserved. Contacts: <stanislav.migunov@outlook.com>
// Created by Stanislav Migunov
package net.mycrysis.launcher.dto

import com.google.gson.annotations.SerializedName

data class DiscordUserData(
    @SerializedName("id")
    val id: String,

    @SerializedName("username")
    val username: String,

    @SerializedName("discriminator")
    val discriminator: String,

    @SerializedName("avatar")
    val avatar: String,

    @SerializedName("public_flags")
    val publicFlags: Int,

    @SerializedName("flags")
    val flags: Int,

    @SerializedName("locale")
    val locale: String,

    @SerializedName("mfa_enabled")
    val mfaEnabled: Boolean

)