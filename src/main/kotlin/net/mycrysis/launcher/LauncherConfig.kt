// Copyright (C) 2019-2020 MYCRYSIS(Unofficial) NET TEAM.
// All rights reserved. Contacts: <stanislav.migunov@outlook.com>
// Created by Stanislav Migunov
package net.mycrysis.launcher

import eu.hgweb.jini.Ini
import javafx.scene.control.Alert
import java.io.File
import kotlin.system.exitProcess

object LauncherConfig {
    private lateinit var configFile: File
    private lateinit var configIni: Ini

    lateinit var launcherVersion: String
    lateinit var buildDirectory:String
    lateinit var localization:String

    fun init() {
        configFile = File("config.ini")

        println(configFile.absolutePath)

        if(!configFile.exists()) {
            LauncherObject.showAlertAndWait(Alert.AlertType.ERROR, "Config Error", null, "Can't open config.ini file")
            exitProcess(-1)
        }

        configIni = Ini(configFile)

        parseValues()
    }

    private fun parseValues() {
        if (!configIni.sectionExists("Game") && !configIni.sectionExists("Launcher")) {
            LauncherObject.showAlertAndWait(Alert.AlertType.ERROR, "Config Error", null, "Bad Config")
            exitProcess(-1)
        }

        val launcherSection = configIni.section("Launcher")

        launcherVersion = launcherSection.value("Version", "null")
        localization = launcherSection.value("Localization", "ENG")

        val gameSection = configIni.section("Game")

        buildDirectory = gameSection.value("Directory", "")

        if(buildDirectory.isNotEmpty())
            //TODO -> Validate Path
            LauncherObject.isGameInstalled = true
    }

    fun saveConfig() {

    }
}