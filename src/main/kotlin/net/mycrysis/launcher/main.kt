// Copyright (C) 2019-2020 MYCRYSIS(Unofficial) NET TEAM.
// All rights reserved. Contacts: <stanislav.migunov@outlook.com>
// Created by Stanislav Migunov

package net.mycrysis.launcher

import de.saxsys.mvvmfx.FluentViewLoader
import de.saxsys.mvvmfx.ViewTuple
import de.saxsys.mvvmfx.easydi.MvvmfxEasyDIApplication
import javafx.application.Application.launch
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.stage.Stage
import kotlinx.coroutines.*
import kotlinx.coroutines.javafx.JavaFx
import net.mycrysis.launcher.View.RootView
import net.mycrysis.launcher.ViewModel.RootViewModel
import net.mycrysis.launcher.http.HttpServer
import kotlin.coroutines.CoroutineContext

fun main() = runBlocking<Unit> {
    launch(JavaFXEntryPoint::class.java)
}

class JavaFXEntryPoint: MvvmfxEasyDIApplication(), CoroutineScope {
    //private lateinit var serverSock: WebSocketServer

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.JavaFx

    override fun startMvvmfx(primaryStage: Stage) {
        LauncherObject.config.init()

        val viewTuple: ViewTuple<RootView, RootViewModel> = FluentViewLoader.fxmlView(RootView::class.java).load()

        val root: Parent = viewTuple.view
        val scene = Scene(root, 700.0, 240.0)

        primaryStage.title = "Launcher"
        primaryStage.scene = scene

        primaryStage.isResizable = false

        primaryStage.show()

        //TODO -> Init Launcher Global

        if(!LauncherObject.isUserAuthentication) {
            launch(Dispatchers.Default + CoroutineName("HttpServer")) {
                HttpServer.init()
            }
        }


//        Thread {
//            serverSock = ServerSock(InetSocketAddress("localhost", 30000))
//            serverSock.run()
//        }.start()
    }

    override fun stopMvvmfx() {
        launch { HttpServer.stop() }
        super.stopMvvmfx()
    }

}