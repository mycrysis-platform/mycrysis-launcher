// Copyright (C) 2019-2020 MYCRYSIS(Unofficial) NET TEAM.
// All rights reserved. Contacts: <stanislav.migunov@outlook.com>
// Created by Stanislav Migunov

package net.mycrysis.launcher.socket

import org.java_websocket.WebSocket
import org.java_websocket.handshake.ClientHandshake
import org.java_websocket.server.WebSocketServer
import java.lang.Exception
import java.net.InetSocketAddress

class ServerSock(address: InetSocketAddress): WebSocketServer(address) {
    private val clients = mutableListOf<ClientObj>()

    override fun onOpen(conn: WebSocket?, handshake: ClientHandshake?) {
        if (conn != null) {
            System.out.println("new connection to " + conn.getRemoteSocketAddress())
        };
    }

    override fun onClose(conn: WebSocket?, code: Int, reason: String?, remote: Boolean) {
        if (conn != null) {
            println("closed " + conn.getRemoteSocketAddress() + " with exit code " + code + " additional info: " + reason)
        }
    }

    override fun onMessage(conn: WebSocket?, message: String?) {
        if (conn != null) {
            println("received message from ${conn.getRemoteSocketAddress()} : $message")
            conn.send("from server: $message")
        }
    }

    override fun onError(conn: WebSocket?, ex: Exception?) {
        if (conn != null) {
            println("an error occurred on connection " + conn.getRemoteSocketAddress()  + ":" + ex)
        }
    }

    override fun onStart() {
        println("server started successfully");
    }


}