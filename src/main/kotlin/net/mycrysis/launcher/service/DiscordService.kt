// Copyright (C) 2019-2020 MYCRYSIS(Unofficial) NET TEAM.
// All rights reserved. Contacts: <stanislav.migunov@outlook.com>
// Created by Stanislav Migunov
package net.mycrysis.launcher.service

import io.ktor.client.*
import io.ktor.client.engine.okhttp.*
import io.ktor.client.features.json.*
import io.ktor.client.request.*
import io.ktor.client.request.forms.*
import io.ktor.http.*
import net.mycrysis.launcher.dto.DiscordAccessData
import net.mycrysis.launcher.dto.DiscordUserData

object DiscordService {
    private val urlApi: String
        get() = "https://discordapp.com/api/"

    private val urlDiscordToken: String
        get() = "https://discordapp.com/api/oauth2/token"

    private val scopes: String
        get() = "identify"

    private val urlRedirect: String?
        get() = "http://127.0.0.1:30333/mycrysis/launcher"

    private val clientId: String?
        get() = "756598414854324316"

    private  val clientSecret: String?
        get() = "3sCEyDtGnTEWcaGyX0RTXR90JfD2HEFP"

    private fun makeHttpClient(): HttpClient = HttpClient(OkHttp) {
        install(JsonFeature) {
            serializer = GsonSerializer()
        }
    }

    suspend fun discordAccessToken(code: String): DiscordAccessData? = try {
        makeHttpClient().request(urlDiscordToken) {
            method = HttpMethod.Post
            body = MultiPartFormDataContent(formData {
                clientId?.let { append("client_id", it) }
                clientSecret?.let { append("client_secret", it) }
                urlRedirect?.let { append("redirect_uri", it) }
                append("grant_type", "authorization_code")
                append("code", code)
                append("scope", scopes)
            })
        }

    } catch (e: Exception) {
        println(e.message)
        null
    }

    suspend fun discordUserData(token: String): DiscordUserData? = try {
        makeHttpClient().request("$urlApi/users/@me") {
            method = HttpMethod.Get
            header("Authorization", "Bearer $token")
        }
    } catch (e: Exception) {
        println(e.message)
        null
    }
}