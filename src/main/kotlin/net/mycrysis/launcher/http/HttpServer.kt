// Copyright (C) 2019-2020 MYCRYSIS(Unofficial) NET TEAM.
// All rights reserved. Contacts: <stanislav.migunov@outlook.com>
// Created by Stanislav Migunov
package net.mycrysis.launcher.http

import de.saxsys.mvvmfx.MvvmFX
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import kotlinx.coroutines.*
import net.mycrysis.launcher.ViewModel.RootViewModel
import net.mycrysis.launcher.dto.DiscordAccessData
import net.mycrysis.launcher.service.DiscordService
import java.util.concurrent.TimeUnit
import kotlin.coroutines.CoroutineContext

object HttpServer: CoroutineScope {
    private lateinit var server: ApplicationEngine
    var isInit = false

    fun init() {
        server = embeddedServer(Netty, port = 30333) {
            routing {
                get("/mycrysis/launcher") {
                    val code = call.parameters["code"]

                    when(code) {
                        null -> {
                            call.respondText("Use 'code' parameter", ContentType.Text.Plain)
                            return@get
                        }
                        "" -> {
                            call.respondText("Code can't be empty", ContentType.Text.Plain)
                            return@get
                        }
                    }

                    val discordAccessData: DiscordAccessData? = DiscordService.discordAccessToken(code.toString())

                    if(discordAccessData == null) {
                        call.respondText("Invalid code", ContentType.Text.Plain)
                        return@get
                    }

                    val discordUserData = DiscordService.discordUserData(discordAccessData!!.accessToken)

                    if(discordUserData == null) {
                        call.respondText("Can't get user data...", ContentType.Text.Plain)
                        return@get
                    }

                    //TODO -> GRPC AUTH

                    MvvmFX.getNotificationCenter().publish(RootViewModel.SIGN_IN_ACTION, discordUserData!!.username)

                    call.respondText("You has been authenticated. Now you can close the tab and open launcher", ContentType.Text.Plain)
                }
            }
        }

        server.start(wait = false)
        isInit = true
    }

    fun stop() {
        server.stop(1, 5, TimeUnit.SECONDS)
        isInit = false
    }

    @ExperimentalCoroutinesApi
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Default + CoroutineName("HttpServer")
}