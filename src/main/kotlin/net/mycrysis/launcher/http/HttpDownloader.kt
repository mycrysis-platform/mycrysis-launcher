// Copyright (C) 2019-2020 MYCRYSIS(Unofficial) NET TEAM.
// All rights reserved. Contacts: <stanislav.migunov@outlook.com>
// Created by Stanislav Migunov
package net.mycrysis.launcher.http

import com.google.gson.Gson
import io.ktor.client.*
import io.ktor.client.engine.okhttp.*
import io.ktor.client.request.*
import io.ktor.client.request.request
import io.ktor.client.statement.*
import io.ktor.http.*
import io.ktor.network.sockets.*
import io.ktor.util.cio.*
import io.ktor.utils.io.*
import kotlinx.coroutines.*
import net.mycrysis.launcher.dto.GameContentModel
import java.io.*
import java.net.ConnectException
import java.net.URI
import java.net.http.HttpRequest

object HttpDownloader {
    private val urlDownload
        get() = "https://gitlab.com/mr-w1lde/mycrysis2build/-/raw/master/"

    private val urlGameContent
        get() = "${urlDownload}content.json"

    private fun ktorClient() = HttpClient(OkHttp) {}

    private suspend fun downloadFile(file: File, url: String, callback: suspend (filename: String, start: Boolean, end: Boolean) -> Unit) {
        val call = ktorClient().request<HttpResponse> {
            url(url)
            method = HttpMethod.Get
        }

        callback(file.name, true, false)

        if(call.status.value == 200) {
            callback(file.name, false, true)
        }

        call.content.copyAndClose(file.writeChannel())
    }

    @Throws(ConnectException::class)
    fun getGameContentInJson(): String {
        val client: java.net.http.HttpClient = java.net.http.HttpClient.newHttpClient()
        val request: HttpRequest = HttpRequest.newBuilder()
            .uri(URI.create(urlGameContent))
            .build()

        return client.send(request, java.net.http.HttpResponse.BodyHandlers.ofString()).body()
    }

    fun downloadBuild(path: String,
                      onDownloading:(description: String, progress: Double) -> Unit,
                      downloadedCallback: (failed: Boolean, msg: String) -> Unit) {
        GlobalScope.launch(Dispatchers.Default + CoroutineName("DownloadingContentCoroutine")) {

            var data: String? = null;
            try {
                data = getGameContentInJson()
            } catch (e: ConnectException) {
                downloadedCallback(true, "ConnectException")
                return@launch
            }
            val listContent: List<GameContentModel> = Gson().fromJson(data, Array<GameContentModel>::class.java).toList()

            var downloaded = 0

            listContent.forEach {
                val filename = "${it.directory}/${it.filename}${it.file_format}"
                val file = File("$path/$filename")

                file.parentFile.mkdir()

                println(urlDownload + filename)
                try {
                    downloadFile(file, urlDownload + filename) { name:String, start: Boolean, end:Boolean ->

                        val progress: Double = downloaded.toDouble() / (listContent.size - 1).toDouble()

                        // TODO -> Localization
                        if(start)
                            onDownloading("Downloading $name", progress)
                        if(end)
                            downloaded += 1

                    }
                } catch (e: SocketTimeoutException) {
                    downloadedCallback(true, "SocketTimeoutException")
                    return@launch
                }
            }

            downloadedCallback(false, "")
        }
    }
}