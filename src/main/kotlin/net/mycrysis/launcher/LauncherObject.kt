// Copyright (C) 2019-2020 MYCRYSIS(Unofficial) NET TEAM.
// All rights reserved. Contacts: <stanislav.migunov@outlook.com>
// Created by Stanislav Migunov
package net.mycrysis.launcher

import javafx.scene.control.Alert

object LauncherObject {
    val config = LauncherConfig

    var isUserAuthentication: Boolean = false
    var isGameButtonActive:Boolean = false
    var isGameInstalled: Boolean = false
    var gameBuild: String = "1.1.1.5620(1) Beta"
    var username: String = "NO USER"

    fun showAlertAndWait(type: Alert.AlertType, title: String, header: String?, msg: String)
    {
        val alert = Alert(type)

        alert.title = title
        alert.headerText = header
        alert.contentText = msg

        alert.showAndWait()
    }

}
