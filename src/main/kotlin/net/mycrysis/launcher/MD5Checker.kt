// Copyright (C) 2019-2020 MYCRYSIS(Unofficial) NET TEAM.
// All rights reserved. Contacts: <stanislav.migunov@outlook.com>
// Created by Stanislav Migunov
package net.mycrysis.launcher

import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.javafx.JavaFx
import kotlinx.coroutines.launch
import net.mycrysis.launcher.dto.GameContentModel
import net.mycrysis.launcher.http.HttpDownloader
import java.io.File
import java.math.BigInteger
import java.net.ConnectException
import java.security.MessageDigest

object MD5Checker {
    private val listFiles: MutableList<File>? = mutableListOf()

    fun getGameFiles(): List<File>? {
        val gameDir = LauncherObject.config.buildDirectory

        if(gameDir.isEmpty()) {
            return null
        }

        val temp = File(gameDir)

        getFileFromDir(temp)
        println("files found: ${listFiles!!.size}")

        return listFiles
    }

    fun MD5(checkCallback: (status: String, complete: Boolean, failed:Boolean) -> Unit)  {
        println("-- MD5 --")
        var data: String? = ""

        try {
            data = HttpDownloader.getGameContentInJson()
        } catch (e: ConnectException) {
            //TODO -> Ex
            e.printStackTrace()
        }

        val listContent: List<GameContentModel> = Gson().fromJson(data, Array<GameContentModel>::class.java).toList()
        var checked = 0

        kotlin.run outForeach@ {
            listFiles?.forEach { gameFile ->
                val fileByteArray = gameFile.readBytes()
                val md5Bytes = MessageDigest.getInstance("MD5").digest(fileByteArray)
                val md5Hex = BigInteger(1, md5Bytes).toString(16).padStart(32, '0')

                listContent.forEach { md5File ->
                    if(gameFile.name == "${md5File.filename}${md5File.file_format}") {
                        //TODO -> Localization
                        val temp = "${md5File.directory}${md5File.filename}${md5File.file_format}"
                        checkCallback("Verifying $temp...", false, false)

                        if(md5Hex != md5File.md5 && md5File.md5 != "*") {
                            checkCallback("$temp is invalid", false, true)
                            println("$temp is invalid: this->$md5Hex, remote->${md5File.md5}")
                            return@outForeach
                        }

                        if(md5File.md5 == "*")
                            checkCallback("$temp skipping for valid", false, false)
                    }
                }

                checked++
                println("${gameFile.name} - $md5Hex - $checked/${listFiles.size}")

                if(checked == listFiles.size) {
                    checkCallback("", true, false)
                    println("Done")
                }
            }
        }
    }

    fun gameCheck( onProgressCallback:(status: String, complete: Boolean, failed:Boolean) -> Unit) {
        GlobalScope.launch {
            getGameFiles()
            MD5 { status, complete, failed ->
                GlobalScope.launch(Dispatchers.JavaFx) {
                    onProgressCallback(status, complete, failed)
                }
            }
        }
    }

    fun getFileFromDir(file: File):File? {
        if(file.isFile) {
            println("${file.name}: ${file.path}")
            listFiles?.add(file)

            return file
        }

        if(file.isDirectory) {
            file.listFiles().forEach { f ->
                (getFileFromDir(f))
            }
        }

        return null
    }
}