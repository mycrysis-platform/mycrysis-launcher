import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import com.google.protobuf.gradle.*

plugins {
    id("java")
    id("application")
    id("org.openjfx.javafxplugin") version "0.0.8"

    id("com.google.protobuf") version "0.8.13"

    id("idea")

    kotlin("jvm") version "1.4.0"
}
group = "net.mycrysis"
version = "0.0.1"

repositories {
    mavenCentral()
    maven(url = "https://maven.hgweb.eu/releases")
}

tasks.withType<KotlinCompile>() {
    kotlinOptions.jvmTarget = "11"
}

dependencies {
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-javafx:1.1.1")

    implementation("org.java-websocket:Java-WebSocket:1.5.1")

    implementation("org.slf4j:slf4j-api:1.7.30")
    implementation("org.slf4j:slf4j-simple:1.7.30")

    implementation("io.ktor:ktor-server-core:1.4.0")
    implementation("io.ktor:ktor-server-netty:1.4.0")

    implementation("io.ktor:ktor-client-core:1.4.0")
    implementation("io.ktor:ktor-client-okhttp:1.4.0")
    implementation("io.ktor:ktor-client-json-jvm:1.4.0")
    implementation("io.ktor:ktor-client-gson:1.4.0")

    implementation("com.google.protobuf:protobuf-java:3.12.4")
    implementation("com.google.protobuf:protobuf-java-util:3.12.4")


    implementation("de.saxsys:mvvmfx:1.8.0")
    implementation("de.saxsys:mvvmfx-easydi:1.8.0")

    implementation("eu.hgweb:jini:1.0.2")
}

application {
    applicationName = "MyCrysis Launcher"
    mainClassName = "net.mycrysis.launcher.MainKt"
}

javafx {
    version = "11.0.2"
    modules = listOf(
        "javafx.base",
        "javafx.controls",
        "javafx.fxml",
        "javafx.graphics",
        "javafx.media",
        "javafx.web"
    )
}

protobuf {
    generatedFilesBaseDir = "$projectDir/generatedProtobuf"
    protoc { artifact = "com.google.protobuf:protoc:3.12.4" }
}

sourceSets {
    main {
        resources {
            setSrcDirs(listOf("src/main/resources"))
        }
    }
}

tasks.withType<Jar> {
    manifest {
        attributes["Main-Class"] = "net.mycrysis.launcher.MainKt"
    }
    configurations["compileClasspath"].forEach { file: File ->
        from(zipTree(file.absoluteFile))
    }
}

tasks {
    "clean"(Delete::class) {
        delete("$projectDir/generatedProtobuf")
    }
}